<?php

use Illuminate\Database\Seeder;
use App\User;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::insert([
        	'name' => 'admin'
        	'email' => 'cath@gmail.com'
        	'role' = 'admin'
        	'is_admin' => 1
        	'password' => Hash::make('password'),

        ]);
    }
}
