<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
Route::get('/catalog', 'ItemController@showItems');
Route::get('/menu/add', 'ItemController@showAddItemForm');
Route::post('/menu/add', 'ItemController@saveItems');
Route::get('/menu/mycart', 'ItemController@showCart');
Route::post('/addToCart/{id}', 'ItemController@addToCart');
Route::delete('/mycart/{id}/delete', 'ItemController@deleteCartItem');
Route::get('/menu/categories/{id}', 'CategoryController@findItems');
Route::get('/menu/genders/{id}', 'GenderController@findItems');
Route::post('/catalog', 'GenderController@findItems');
Route::get('/checkout', 'ItemController@checkout');
Route::get('/orders', 'ItemController@showOrders');
Route::get('/home', 'SearchController@searchItems');
Route::get('/search', 'ItemController@search');
Route::post('/catalog', 'ItemController@search');
Route::patch('/mycart/{id}/changeQty', 'ItemController@changeItemQty');



Route::get('/transaction_complete', 'ItemController@checkout');
Route::get('/menu/{id}/edit', 'ItemController@showEditForm');
Route::patch('/menu/{id}/edit', 'ItemController@saveEdit');

Route::get('/menu/{id}', 'ItemController@itemDetails');
Route::delete('/menu/{id}/delete', 'ItemController@deleteItem');
Route::delete('/cart/clearcart', 'ItemController@clearCart');




Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');


