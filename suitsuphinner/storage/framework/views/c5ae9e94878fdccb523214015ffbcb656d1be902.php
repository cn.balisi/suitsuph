;

<?php $__env->startSection('title', 'Order Confirmation'); ?>

<?php $__env->startSection('content'); ?>
	<div class="container">
		<div class="jumbotron">
			<h2>Thank you for shopping with us!</h2>
			<p>Your payment will be verified within the day. Please proceed to <a href="<?php echo e(url('/orders')); ?>">your order history</a>
			to check the status of your order.</p>
		</div>

	</div>

<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\Users\Lenovo\suitsucath\resources\views/orders/order_confirmation.blade.php ENDPATH**/ ?>