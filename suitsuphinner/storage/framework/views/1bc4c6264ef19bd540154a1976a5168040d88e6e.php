<?php $__env->startSection('content'); ?>
<div class="itemd container-fluid">
	<div class="row">
		<div class="col-4 offset-2 itemdetails">

	<img src="/<?php echo e($item->image_url); ?>" class="img-fluid d-block">
	</div>
	<div class="col-4 itemdetails2 md-0">

	<h1>Item Details </h1>
	
	<p>Item Name: <?php echo e($item->name); ?></p>
	<p>Item Description:<?php echo e($item->description); ?></p>
	<p>Item Price: <?php echo e($item->price_rent); ?> </p>
	<p>Item Price: <?php echo e($item->price_purchase); ?> </p>
	<a href="/menu/<?php echo e($item->id); ?>/edit" class="btn-outline-light btn btn-lg">Edit</a>

	<a href="<?php echo e(url('/catalog')); ?>" class="btn-outline-light btn btn-lg">Back</a>
	<button class="btn btn-danger btn-lg" data-toggle="modal" data-target="#confirmDelete">Delete</button>
	 
	</div>
  </div>
	
	<div id="confirmDelete" class="modal fade" role="dialog">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<h4>Confirm Delete</h4>
				</div>

				<div class="modal-body">
					<p>Are you sure you want to delete?</p>
				</div>

				<div class="modal-footer">
					<button type="button" class="btn btn-light" data-dismiss="modal">Cancel</button>
					
					<form action="/menu/<?php echo e($item->id); ?>/delete" method="POST">
						<?php echo csrf_field(); ?>
						<?php echo e(method_field("DELETE")); ?>

						<button type="submit" class="btn btn-danger">Confirm</button>
					</form>

				</div>
			</div>
		</div>
	</div> 

	<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
	<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>



<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\Users\Lenovo\Desktop\suitsucath\resources\views/items/item_details.blade.php ENDPATH**/ ?>