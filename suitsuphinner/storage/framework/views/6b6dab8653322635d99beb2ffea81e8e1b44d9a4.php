;

<title>ORDER CONFIRMATION || SUITS.U</title>
    <link rel="icon" alt class="titlelogo"type="images/png" href="../images/suitsulogoblack2.png">

<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
 <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.2/css/all.css" integrity="sha384-oS3vJWv+0UjzBfQzYUhtDYW+Pj2yciDJxpsK1OYPAYjqT085Qq/1cq5FLXAZQ7Ay" crossorigin="anonymous">

<!-- <?php $__env->startSection('title', 'Order Confirmation'); ?> -->

<?php $__env->startSection('content'); ?>
	<div class="container orderconfi">
		<div class="jumbotron">
			<h2>Thank you for shopping with us!</h2>
			<p>Your payment will be verified within the day. Please proceed to <a href="<?php echo e(url('/orders')); ?>">your order history</a>
			to check the status of your order.</p>
		</div>

	</div>

<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\Users\Lenovo\Desktop\ANYTHING\suitsuph\suitsuphinner\resources\views/orders/order_confirmation.blade.php ENDPATH**/ ?>