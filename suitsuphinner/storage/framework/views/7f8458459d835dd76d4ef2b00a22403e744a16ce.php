<title>CONTACT US || SUITS.U</title>
    <link rel="icon" alt class="titlelogo"type="images/png" href="../images/suitsulogoblack2.png">

<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
 <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.2/css/all.css" integrity="sha384-oS3vJWv+0UjzBfQzYUhtDYW+Pj2yciDJxpsK1OYPAYjqT085Qq/1cq5FLXAZQ7Ay" crossorigin="anonymous">
 <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
 <script src="https://cdn.jsdelivr.net/npm/sweetalert2@8"></script>
 <script src="<?php echo e(asset('css/css/animate.css')); ?>"defer></script>
 <link rel="stylesheet" type="text/css" href="./assets/css/animate.css">
  <link rel="stylesheet" type="text/css" href="./assets/css/sweetalert2.css">
  <script src="<?php echo e(asset('css/sweetalert2.css')); ?>"></script>
  <script src="<?php echo e(asset('js/sweetalert2.min.js')); ?>"defer></script>
  
<?php $__env->startSection('content'); ?>

<header id="head-menu">
			<h4 class="text-left h">CONTACT US</h4>
		</header>

		<div class="container-fluid">
		<div class="row">
			<div class="col-md-6">
          <div id="cu">For Renting: Please contact or drop by at our office.</div>
				<form>
					<div class="form-group">
						<label for="email"> Email: </label>
						<input type="email" id="email" class="form-control" placeholder="Enter your Email here" required>
						<span id="emailErr" class="alert"></span>
					</div> <!-- end form group -->

					<div class="form-group">
						<label for="message"> Message: </label>
						<textarea id="message" class="form-control" placeholder="Enter your Message Here" rows="5" required></textarea>
						<span id="msgErr" class="alert"></span>
					</div> <!-- end form group -->

					<button id="sub" class="btn btn-block btn-success">Submit Form</button>
				</form>

				<div class="delivery">
					<h1 id="wedo">We do delivery (COD)</h1>
					<h5 class="call">Call us at:</h5>
					<h5><i class="fas fa-mobile-alt"> 0926-8845456</i></h5>

				</div>
			</div> <!-- end form -->

			<div class="col-md-6">
				<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3860.9246347116373!2d120.97114674986457!3d14.603368989750633!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3397ca0fb24218d9%3A0xe225aef117f273cc!2sLucky+Chinatown+Mall%2C+Lachambre+St%2C+Binondo%2C+Manila%2C+1006+Metro+Manila!5e0!3m2!1sfil!2sph!4v1560960032026!5m2!1sfil!2sph" width="600" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
			</div> 

		</div> <!-- end row-->

    <script src="<?php echo e(asset('js/sweetalert2.min.js')); ?>"defer></script>
      

<script type="text/javascript">
 document.querySelector("#sub").addEventListener("click", function() {
  let errors = 0;
  let email = document.querySelector("#email").value;
  let message = document.querySelector("#message").value;



if(!email.includes("@")) {
  document.querySelector("#emailErr").innerHTML = "Please provide a valid email";
  errors++;
} else {
  document.querySelector("#emailErr").innerHTML = " ";
}
if(message.length < 1) {
  document.querySelector("#msgErr").innerHTML = "Please Input message";
  errors++;
} else {
  document.querySelector("#msgErr").innerHTML = " ";
}

if(errors > 0) {
  Swal.fire({
      type: 'error',
     title: 'Oops...',
      text: 'Please complete details!',
});
  
  document.querySelector("#email").value = " ";
  document.querySelector("#message").value = " ";
  errors++;
  return false;
 } else {
  Swal.fire({
  title: 'Nice to hear your message!',
  text: 'Expect our response within the day.',
  imageUrl: '../images/thank you.jpg',
  imageWidth: 400,
  imageHeight: 200,
  imageAlt: 'Custom image',
  animation: false
  });
  
  document.querySelector("#email").value = " ";
  document.querySelector("#message").value = " ";

}
 });
</script>


<!-- <div class="row">
  <div class="col-12 px-0 container-fluid">
  <div id="carouselExampleCaptions" class="carousel slide carousel-fade" data-ride="carousel">
    <div class="carousel-inner container-fluid py-0 px-0 mb-0">
      <div class="carousel-item  active">
        <img src="../images/cindbc.jpg" class="home caritem1 container-fluid" alt="...">
        <div class="carousel-caption d-none d-md-block">
         <h5 class="cat-head">PROUD</h5>
      <p id="cat-p">wearing someone you've got to...</p>
        </div>
      </div>
      <div class="carousel-item">
        <img src="../images/m72.jpg" class="home container-fluid" alt="...">
        <div class="carousel-caption d-none d-md-block">
          <h5 class="cat-head2">POSSESS</h5>
        </div>
      </div>
      <div class="carousel-item caritem3">
        <img src="../images/back4.jpg" class="home caritem3 container-fluid" alt="...">
        <div class="carousel-caption d-none d-md-block">
          <h5 class="cat-head">CHOOSE</h5>
          <p id="cat-p2">your costumes</p>
          <a href="<?php echo e(url('/catalog')); ?>" class="btn-outline-light btn btn-lg">See costumes</a>   


        </div>
      </div>
    </div>
  
  </div>
  </div> -->

         <!-- <div class="alrt">
                    <?php if(session('status')): ?>
                        <div class="alert alert-success" role="alert">
                            <?php echo e(session('status')); ?>

                        </div>
                    <?php endif; ?>    -->

            <!-- <div class="head1 container"> 
                <h1 class=>WELCOME HOME!</h1>
                 <p>Events are coming...</p> 
                        <p>Play must be prepared...</p> 
                          <p>Stress on what to dress?</p> 
                      </p>Not anymore! </p>
                        <p>Pick and dress up what you feel what</p> SUITS U...  -->

                                      
<!-- 
                      
                </div>
                </div>
            </div> -->

<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\Users\Lenovo\Desktop\suitsucath\resources\views/home.blade.php ENDPATH**/ ?>