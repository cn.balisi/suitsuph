 <title>CATALOG || SUITS.U</title>
    <link rel="icon" alt class="titlelogo"type="images/png" href="../images/suitsulogoblack2.png">

<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
 <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.2/css/all.css" integrity="sha384-oS3vJWv+0UjzBfQzYUhtDYW+Pj2yciDJxpsK1OYPAYjqT085Qq/1cq5FLXAZQ7Ay" crossorigin="anonymous">
 <script src="<?php echo e(asset('css/animate.css')); ?>"defer></script>
 <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
 <link rel="stylesheet" type="text/css" href="../css/animate.css">

<!-- <script src="<?php echo e(asset('js/catalog.js')); ?>"></script>

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.0/jquery.min.js"></script>
    <link href="<?php echo e(asset('css/app.css')); ?>" rel="stylesheet"> -->
<!--  <style>
html {

width:100%;
height:100%;  
margin:0;
padding:0;
   
}

body {
 
margin:0;
width:100%;
height:100%;
padding:0;
font-family:Arial, sans-serif;
background:black;
background-image: radial-gradient(circle at 93% 85% , rgb(250, 0, 100), transparent 100%), radial-gradient(circle at 95% 3% , rgb(242, 0, 255), transparent 100%), radial-gradient(circle at 12% 65% , rgb(147, 0,191), transparent 100%);


  
}

canvas {


display:block;
background-color: black;


}

#animation {

margin:0;
width:100%;
height:100%;
padding:0;
font-family:Arial, sans-serif;
background:black;
background-image: radial-gradient(circle at 93% 85% , rgb(250, 0, 100), transparent 100%), radial-gradient(circle at 95% 3% , rgb(242, 0, 255), transparent 100%), radial-gradient(circle at 12% 65% , rgb(147, 0,191), transparent 100%);
position: absolute;
left: 0; 
top: 0;
width:100%;
height:100%;
margin:0;
padding:0;
display:block;
 }

</style>
 -->


<?php $__env->startSection('content'); ?>
<div class="row">
  <div class="col-12 px-0 container-fluid">
  <div id="carouselExampleCaptions" class="carousel slide carousel-fade" data-ride="carousel">
  <div id="carouselExampleControls" class="carousel slide" data-ride="carousel">
    <div class="carousel-inner container-fluid py-0 px-0 mb-0">
      <div class="carousel-item  active">
        <img src="../images/cindbc.jpg" class="home caritem1 container-fluid" alt="...">
        <div class="carousel-caption d-none d-md-block">
         <h5 class="cat-head animated slideInDown">PROUD</h5>
      <p id="cat-p" class="animated slideInUp">wearing someone you've got to...</p>
        </div>
      </div>
      <div class="carousel-item">
        <img src="../images/m72.jpg" class="home container-fluid" alt="...">
        <div class="carousel-caption d-none d-md-block">
		  <h5 class="cat-head2 animated fadeInLeft">POSSESS</h5>
		  <p id="cat-p1" class="animated fadeInRight">a look that suits u...</p>
        </div>
      </div>
      <div class="carousel-item caritem3">
        <img src="../images/back4.jpg" class="home caritem3 container-fluid" alt="...">
        <div class="carousel-caption d-none d-md-block">
          <h5 class="cat-head1 animated zoomIn ">CHOOSE</h5>
          <p id="cat-p2" class="animated zoomIn">your costume</p>
          <a href="<?php echo e(url('/catalog')); ?>" class="btn-outline-light btn-see btn-lg">See costumes</a>   

        </div>
      </div>
	</div>
	<a class="carousel-control-prev" href="#carouselExampleControls" role="button" data-slide="prev">
    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
    <span class="sr-only">Previous</span>
  </a>
  <a class="carousel-control-next" href="#carouselExampleControls" role="button" data-slide="next">
    <span class="carousel-control-next-icon" aria-hidden="true"></span>
    <span class="sr-only">Next</span>
  </a>
</div>
  </div>
  </div>
  <div class="bar container-fluid"></div>
<div class="cartcatalog container">


 		<!-- <div class="alert2 container-fluid mt-0 pt-0">
 			<h5 id="alert2">You have added: </h5>
 					
		<?php if(Session::has('success_message')): ?>
			<div class="alert2 alert-mes">
				<?php echo e(Session::get('success_message')); ?>

			</div>
		<?php endif; ?>
		<?php if(Session::has('success_message_cart')): ?>
			<div class="alert2 alert-mes">
				<p id="success"><?php echo e(Session::get('success_message_cart')); ?> </p>
			</div>
		<?php endif; ?>	

 		</div> -->

	<!-- 	<div class="row">
			<div class="alert col-md-2">
					
		<?php if(Session::has('success_message')): ?>
			<div class="alert alert-mes">
				<?php echo e(Session::get('success_message')); ?>

			</div>
		<?php endif; ?>
		<?php if(Session::has('success_message_cart')): ?>
			<div class="alert alert-mes">
				<p id="success"><?php echo e(Session::get('success_message_cart')); ?> </p>
			</div>
		<?php endif; ?>
					
 -->
 		<div class="row" id="dropdowns">
 			<div class="col-md-3 mt-3 mb-3 py-0">
			<div class="dropdown">
			<a class="list dropdown"  href="/catalog">All</a>
			</div>
			 </div>
			 <div class="col-md-3 mt-3 mb-3 py-0">
			<div class="dropdown">
			<a class="list dropdownbtn" id="categorymenu" aria-haspopup="true" aria-expanded="false">By Category</a>
			<div class="dropdown-content">
      		
			<?php $__currentLoopData = \App\Category::all(); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $category): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
			
			<a class="drop-item" href="/menu/categories/<?php echo e($category->id); ?>"><?php echo e($category->name); ?></a>

			<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
			</div>
			</div>
			</div>
			<div class="col-md-3 mt-3 mb-3 py-0">
			<div class="dropdown">
			<a class="list dropdownbtn" id="gendermenu" aria-haspopup="true" aria-expanded="false">By Gender</a>
			<div class="dropdown-content" arialabelledby="gendermenu">
			<?php $__currentLoopData = \App\Gender::all(); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $gender): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
			
			<a class="drop-item" href="/menu/genders/<?php echo e($gender->id); ?>"><?php echo e($gender->name); ?></a>

			<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

			</div>
			</div>
			</div>

			
                         
			<div class="col-md-3 mt-3 mb-3 py-0">
                            <div class="d-inline">
                              <form action="/catalog" class="d-inline" method="POST"> 
                                  <?php echo csrf_field(); ?>
                        <input type="text" class="form-control d-inline" name="search" placeholder="Search Costume"> <span class="input-group-btn"></span>
                        
                        <button type="submit" class="btn btn-warning">
                            <span class='glyphicon bg-warning glyphicon-search'></span> <i class="fas fa-search"></i>
                        </button>
                        </form>
                      </div>
                        </div>
                         	
                

	</div>
	
	 	
			<div class="row">
				<?php $__currentLoopData = $items; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $indiv_item): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
				   	<div class="col-3" id="catitem">
				     		<div class="card-catalog">
				     			<img src="/<?php echo e($indiv_item->image_url); ?>" alt= "" class="card-img-top">
										 <h4 class="card-title"><?php echo e($indiv_item->name); ?></h4>
										 <p><?php echo e($indiv_item->description); ?></p>
				     					<!-- <form action="/<?php echo e($indiv_item->price_purchase); ?>disabled" method="POST">
				     					
											<?php echo csrf_field(); ?>
				     					<button class="btn btn-success btn-sm price"> RENT</button>
				     					
				     					
				     					 <form action="/<?php echo e($indiv_item->price_rent); ?>/delete">
											<?php echo csrf_field(); ?>
				     					<button class="btn btn-success btn-sm price" onclick="/<?php echo e($indiv_item->price_rent); ?>disabled"> PURCHASE</button> --> 
				     					<!-- <h5>₱<?php echo e(number_format($indiv_item->price_rent,2)); ?></h5> -->
										 <h5 class="pricetag">₱<?php echo e(number_format($indiv_item->price_purchase,2)); ?></h5>
									 
										 
										 
									<?php if(auth()->guard()->check()): ?>
									<?php if(auth()->user()->is_admin == 1): ?>
				     				<a class="btn btn-md btn-dark" href="/menu/<?php echo e($indiv_item->id); ?>" > <i class="far fa-eye"> </i>View details</a>
									<p> only "<?php echo e($indiv_item->stocks); ?>" left</p>
									<?php endif; ?>
									<?php endif; ?>
				     				<form action="/addToCart/<?php echo e($indiv_item->id); ?>" method="POST">
									<?php echo csrf_field(); ?>
									<div class="mr-sm-2">
									<?php if((Auth::user() != null)&&(auth()->user()->is_admin != 1)): ?>
									<input type="number" name="quantity" class="quantity form-control-sm" min="1" placeholder="Qty">
									<button class="btn btn-md btn-outline-secondary addtocart" type="submit"> <i class="fas fa-shopping-cart"></i></button>
									<?php else: ?>
									<input type="number" name="quantity" class="quantity form-control-sm" min="1" placeholder="Qty">
									<a class="btn btn-md btn-outline-secondary addtocart" href="<?php echo e(url('/login')); ?>"> <i class="fas fa-shopping-cart"></i></a>	
									<?php endif; ?>
									</form>
									
				     		</div>
				     		</div>

				    		</div> 
				     <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>  
				      
				    </div>   
			</div>

</div>


<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>


<script>
window.addEventListener('load', function() {
if (document.querySelector('#success').innerHTML == "") {
console.log(document.querySelector("#success"));
console.log('hello')
} else {
document.querySelector("#success").classList.add('hide-success');
document.querySelector(".alert-success").classList.add('hide-success');
}


})
</script>


<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\Users\Lenovo\Desktop\ANYTHING\suitsuph\suitsuphinner\resources\views/items/catalog.blade.php ENDPATH**/ ?>