<!DOCTYPE html>
<html lang="<?php echo e(str_replace('_', '-', app()->getLocale())); ?>">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="<?php echo e(csrf_token()); ?>">

    <title>SUITS.U</title>
    <link rel="icon" alt class="titlelogo"type="images/png" href="../images/suitsulogoblack2.png">

    <!-- Scripts -->
     <script src="<?php echo e(asset('js/app.js')); ?>"></script>
     
     <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">

   
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Pacifico&display=swap" rel="stylesheet">

    <!-- Styles -->
    <link href="<?php echo e(asset('css/app.css')); ?>" rel="stylesheet">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.2/css/all.css"integrity="sha384-oS3vJWv+0UjzBfQzYUhtDYW+Pj2yciDJxpsK1OYPAYjqT085Qq/1cq5FLXAZQ7Ay" crossorigin="anonymous">


</head>
<body>
    <div id="navup">
        <div class="navbar">
        <nav class="navbar navbar-expand-lg shadow-sm navbar-dark fixed-top">
            <div class="container">
                <a class="navbar-brand" href="<?php echo e(url('/')); ?>">
                  <img src="../images/suitsulogowhite2.png" alt class="logo">
                </a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="<?php echo e(__('Toggle navigation')); ?>">
                    <span class="navbar-toggler-icon"></span>
                </button>

                
                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                  
                     

                    <!-- Right Side Of Navbar -->
                    <ul class="navbar-nav ml-auto">
                        <!-- Authentication Links -->
                        

                         <li class="nav-item">
                          <a class="navbar" href="<?php echo e(url('/')); ?>">HOME</a>
                          </li>
                          <li class="nav-item">
                          <a class="navbar" href="<?php echo e(url('/catalog')); ?>">COSTUMES</a>
                          </li>
                          <li class="nav-item">
                          <a class="navbar" href="<?php echo e(url('/home')); ?>">CONTACTS</a>
                          </li>
                          <?php if(auth()->guard()->check()): ?>
                        <?php if(auth()->user()->is_admin == 1): ?>
                        <li class="nav-item">
                          <a class="navbar" href="<?php echo e(url('/menu/add')); ?>">ADD ITEM</a>
                          </li>
                        <?php endif; ?>
                        <?php endif; ?>
                        <?php if(auth()->guard()->check()): ?>
                        <?php if(auth()->user()->is_admin != 1): ?>
                           <li class="nav-item">
                                <a class="navbar" href="<?php echo e(url('/menu/mycart')); ?>">CART
                                    <span class="badge badge-dark text-light text-center">
                                    <?php if(Session::has('cart')): ?>
                                    <?php echo e(array_sum(Session::get('cart'))); ?>

                                    <?php endif; ?>
                                    </span>
                                </a>
                            </li>
                            <?php endif; ?>
                            <?php endif; ?>
                            <!-- <li class="serchbtn active"> </span> <i class="fas fa-search" aria-hidden></i> -->
                               <?php if(auth()->guard()->guest()): ?>

                         <li class="nav-item dropdownAccount">
                                <a id="navbardropdownaccount" class="navbar dropdown-toggle" href="#"  data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                                    MY ACCOUNT <span class="caret"></span>
                                </a>
                                <div class="dropdown-menu  dropdown-menu-right" aria-labelledby="navbarDropdown">
                            
                                <a class="nav-link text-dark text-center dropdown-item" href="<?php echo e(route('login')); ?>">LOGIN</a>
                            

                            <?php if(Route::has('register')): ?>
                                
                                    <a class="nav-link text-dark text-center dropdown-item" href="<?php echo e(route('register')); ?>">REGISTER</a>
                                
                            <?php endif; ?>
                          

                        <?php else: ?>
                             <li class="nav-item dropdown">
                                <a id="navbarDropdown" class="navbar dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                                  LOGOUT   <span class="caret"></span>
                                </a>

                                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                                    <a class="dropdown-item" href="<?php echo e(route('logout')); ?>"
                                       onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                        <?php echo e(Auth::user()->name); ?>

                                    </a>

                                    <form id="logout-form" action="<?php echo e(route('logout')); ?>" method="POST" style="">
                                        <?php echo csrf_field(); ?>
                                    </form>
                                </div>
                            </li>
                        <?php endif; ?>

                       
                        </div>
                 

                       
                    </ul>

                </div>
                
                   
                    

                </div>
                                    
            </div>
        </nav>

        <main class="py-0 px-0 container-fluid">
            <?php echo $__env->yieldContent('content'); ?>
          <footer class="footer fixed-bottom text-center py-0">
            <div class="row">
              <div class="col-12 social-media">
                <ul id="social-media">
                <a class="social-media" href="https://www.facebook.com/apple.nitullano/"><i class="fab fa-facebook-square"></i></a>
               <a class="social-media" href="https://gitlab.com/cn.balisi/"><i class="fab fa-gitlab"></i></a>
                <a class="social-media" href="https://www.instagram.com/toyotailovedeals/"><i class="fab fa-instagram"></i></a>
                </ul>
          <p class="text-left1">
            SUITS.U 2019 &copy; Copyright CNB
          </p>
          </footer>
            <?php echo $__env->yieldContent('title'); ?>
        </main>

         <?php echo $__env->yieldContent('script'); ?>
        </div>  
    </div>
   
    
   
          <!-- jQuery library -->
    <!-- <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.0/jquery.min.js"></script> -->

    <!-- Popper JS -->
    <!-- <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script> -->

    <!-- Latest compiled JavaScript -->
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script> 
    

    <!-- Fonts -->
    


</body>
</html>
<?php /**PATH C:\Users\Lenovo\Desktop\ANYTHING\suitsuph\suitsuphinner\resources\views/layouts/app.blade.php ENDPATH**/ ?>