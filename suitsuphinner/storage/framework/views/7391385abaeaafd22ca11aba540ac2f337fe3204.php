;
<?php $__env->startSection('title', 'Order Confirmation'); ?>
<?php $__env->startSection('content'); ?>
<div class="container">
	<div class="row">
		<div class="col">
			<table class="table table-hover">
				<thead>
					<tr>
						
						<th scope="col">Order #</th>
						<th scope="col">Date Purchased</th>
						<th scope="col">Total</th>
						<th scope="col">Stats</th>
					</tr>
				</thead>
				<tbody>
						<?php $__currentLoopData = $orders; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $order): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
					<tr>
						<td><?php echo e($order->id); ?></td>
						<td><?php echo e($order->created_at); ?></td>
						<td><?php echo e(number_format($order->total,2)); ?></td>
						<td><?php echo e($order->status->name); ?></td>
					</tr>
					<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
				</tbody>
			</div>
		</div>
	</div>
</div>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\Users\Lenovo\Desktop\ANYTHING\suitsuph\suitsuphinner\resources\views/orders/order_history.blade.php ENDPATH**/ ?>