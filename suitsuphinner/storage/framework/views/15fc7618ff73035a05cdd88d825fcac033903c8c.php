<!-- <title>EDIT| SUITS.U</title>
 <link rel="icon" alt class="titlelogo" type="images/png" href="../images/suitsulogoblack2.png"> -->

<?php $__env->startSection('content'); ?>


	<div class=" wall container-fluid">
		<h1 class="edittitle">Edit Item</h1>
		<div class="row">
			<div class="col-md-8 mx-auto">
				<form action="/menu/<?php echo e($item->id); ?>/edit" method="POST" enctype="multipart/form-data">
					<?php echo csrf_field(); ?>
					<?php echo e(method_field("PATCH")); ?>

					<div class="form-group">
						<label for="name">Name:</label>
						<input type="text" name="name" id="name" class="form-control" value="<?php echo e($item->name); ?>">
					</div>

					<div class="form-group">
						<label for="description">Description:</label>
						<input type="text" name="description" id="description" class="form-control" value="<?php echo e($item->description); ?>">
					</div>

					<div class="form-group">
						<label for="price_rent">Price rent:</label>
						<input type="text" name="price_rent" id="price_rent" class="form-control">
					</div>

					<div class="form-group">
						<label for="price_purchase">Price purchase:</label>
						<input type="text" name="price_purchase" id="price_purchase" class="form-control">
					</div>

					<div class="form-group">
						<label for="stocks">Stocks:</label>
						<input type="number" name="number" id="stocks" class="form-control" min="1">
					</div>

					<div class="form-group">
						<label for="size">Size:</label>
						<input type="text" name="size" id="size" class="form-control">
					</div>


					<div class="form-group">
						<label for="gender">Gender:</label>
						<select name="gender" id="gender" class="form-control">
							<?php $__currentLoopData = $genders; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $gender): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
								<option value="<?php echo e($gender->id); ?>"> <?php echo e($gender->name); ?></option>
							<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
						</select>
					</div>

					<div class="form-group">
						<label for="category">Category:</label>
						<select name="category" id="category" class="form-control">
							<?php $__currentLoopData = $categories; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $category): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
								<option value="<?php echo e($category->id); ?>"  
									<?php echo e($category->id == $item->category_id ? "selected" : ""); ?>>
										<?php echo e($category->name); ?>

								</option>
							<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
						</select>
					</div>

					<div class="form-group">
						<label for="image">Upload Image</label>
						<input type="file" name="image" id="image" class="form-control">
					</div>

					<button class="btn btn-success" type="submit">Edit Item</button>					
				</form>
			</div>
		</div>
	</div>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\Users\Lenovo\Desktop\suitsucath\resources\views/items/edit_form.blade.php ENDPATH**/ ?>