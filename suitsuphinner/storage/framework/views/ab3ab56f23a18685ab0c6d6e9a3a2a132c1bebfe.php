<?php $__env->startSection('content'); ?>

<?php if(Session::get('cart') != []): ?>
<table class="table text-center col-md-8 offset-2">
	<thead>
		<tr>
			<th scope="col">Items</th>
			<th scope="col">Quantity</th>
			<th scope="col">Price_purchase</th>
			<th scope="col">Subtotal</th>
			<th scope="col">Action</th>
		</tr>
	</thead>
	<tbody>
		
		<?php $__currentLoopData = $item_cart; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $item): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
		<tr>
			<td> <?php echo e($item->name); ?></td>
			<td>
			<form method="post" id="update_quantity<?php echo e($item->id); ?>">
                  <?php echo csrf_field(); ?>
                  <?php echo e(method_field('PATCH')); ?>


                  <div class="input-group mb-3">
                    <div class="input-group-prepend">
                      <button type="button" class="input-group-text rounded-0" onclick="minus(<?php echo e($item->id); ?>)">&#8722;</button>
                    </div>
                    
                   <input type="number" min="1" value="<?php echo e($item->quantity); ?>" name="quantity" class="text-center" id="quantity<?php echo e($item->id); ?>" style="width:15%;"readonly>
                    <div class="input-group-append">
                      <button type="button" class="input-group-text rounded-0" onclick="plus(<?php echo e($item->id); ?>)">+</button>
                    </div>
                  </div>
                </form>
			</td>
			<td>₱<?php echo e(number_format($item->price_purchase,2)); ?></td>
			<td>₱<?php echo e(number_format($item->subtotal,2)); ?></td>
			<td>
				<button class="btn btn-danger"
				onclick="openDeleteModal('<?php echo e($item->id); ?>', '<?php echo e($item->name); ?>')"
				data-toggle="modal"
				data-target="#delete_modal"> <i class="fas fa-trash-alt"></i>
				
				</button>
			</td>
		</tr>
		
		<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
		
		<tr>
			<td></td>
			<td></td>
			<td></td>
			<td>₱<?php echo e(number_format($total,2)); ?></td>
			<td>


				<div class="row">
					<div class="col">
						<div class="d-flex flex-row">
							<a href="/catalog" class="btn btn-primary mr-2">Back To Shopping</a>			
							<form action="/cart/clearcart" method="post">
								<?php echo csrf_field(); ?>
								<?php echo e(method_field('DELETE')); ?>

								<button type="submit" class="btn btn-outline-danger">Clear Cart</button>
							</form>
							
						</div>
						<div>
							<a onclick="pay()" class="btn btn-success mr-2">Checkout COD</a>
						</div>

					</div>
				</div>
				
			</td>
		</tr>
	</tbody>
</table>

	
	<?php else: ?>
	<h1>test</h1>
	<?php endif; ?>
	
	<?php if($item_cart == null): ?>
	<div class="container jumbotron emptycart">
		<div class="row">
			<div class="col-12 text-center">
				<h4>Your cart is empty</h4>
			</div>
		</div>
		<div class="row">
			<div class="col-12 text-center">
				<a href="/catalog" class="btn btn-dark shopnow">Shop Now!</a>
			</div>
		</div>
	</div>
	<?php endif; ?>
	
	<div class="modal fade text-dark" tabindex="-1" role="dialog" id="delete_modal">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<h5 class="modal-title">Delete Item</h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
					</button>
				</div>
				<div class="modal-body ">
					<p>Are you sure you want to delete ?</p>
				</div>
				<div class="modal-footer">
					<div class="d-flex flex-row">
			          <form method="post" id="delete_form">
			            <?php echo csrf_field(); ?>
			            <?php echo e(method_field('DELETE')); ?>

			            <button class="btn btn-danger" 
			            type="submit">Delete Item</button>
			          </form>
					<button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
					</form>
				</div>
			</div>
		</div>
	</div>

	<?php if(Session::has('cart')): ?>
	<script src="https://js.stripe.com/v3/"></script>

	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.0/jquery.min.js"></script>
	

	<script>

		const stripe = Stripe('pk_test_weXddW8LB0NV1tJZZygG0QDy00y5XkF3Sp');

		function pay() {
			// alert('hello');

			stripe.redirectToCheckout({
			  sessionId: '<?php echo e($CHECKOUT_SESSION_ID); ?>'
			}).then((result) => {
				$('error_modal').modal('show');

			});
		}
	</script>
	<?php endif; ?>
	<script>
		function openDeleteModal(id, name) {
		$('#delete_form').attr('action', '/mycart/' + id + '/delete');
		$('#delete_question').html("Do you want to delete " + name + "?");

		document.querySelectorAll(".subDel").forEach(function(subD) {
			console.log(subD.parentNode.childNodes[2])
		})
		}
	</script>
	
	

<?php $__env->stopSection(); ?>



<?php echo $__env->make('layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\Users\Lenovo\Desktop\suitsuph\resources\views/items/cart_content.blade.php ENDPATH**/ ?>