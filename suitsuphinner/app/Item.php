<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Item extends Model
{
  public function category() {
  	//use the keyword belongs to the table that holds the foreign key
     return $this->belongsTo("\App\Category");

  }

  public function gender() {
  	//use the keyword belongs to the table that holds the foreign key
     return $this->belongsTo("\App\Gender");

  }

   public function orders() {
   	return $this->belongsToMany('\App\Order')->
   	withPivot('quantity')->withTimeStamps();
   }

}
