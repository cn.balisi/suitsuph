<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Item;
use App\Category;
use App\Gender;
use Session;
use Auth;
use App\Order;
use App\Status;
use App;

class ItemController extends Controller
{
    public function showItems() {
        $items = Item::all();
        // dd($items);
        return view('items.catalog', compact('items'));
    }

     public function search(Request $request)
    {
        // dd($request->search);
        $search = $request->search;
        $items = Item::query()->where('name', 'LIKE', "%{$search}%")->get();
        return view('items.catalog', compact('items'));
    }

     public function findItems($id){
        $category = Category::find($id);
        //dd($category);
        $items = $category->items;
        //this enables us to SELECT ALL ITEMS for category
        //this is the effect of the hasMany items() relationship declaration
        return view('items.catalog', compact('items'));
    }

    public function itemDetails($id) {
        // dd($id);
        $item = Item::find($id);
        return view('items.item_details', compact("item"));
    }

    public function deleteItem($id) {
        // dd($id);
        $item = Item::find($id);
        $item->delete();
        return redirect('/catalog');
    }

    public function showAddItemForm() {
        $categories = Category::all();
         $genders = Gender::all();
        return view('items.add_item', compact(['categories', 'genders']));
    }


    public function saveItems(Request $request) {
        // dd($request);
        $item = new Item;
        $item->name = $request->name;
        $item->description = $request->description;
        $item->price_rent = $request->price_rent;
        $item->price_purchase = $request->price_purchase;
        $item->stocks = $request->stocks;
        $item->category_id = $request->category;
        $item->gender_id = $request->gender_id;
        $item->size_id = $request->size_id;

        // dd($item);
        // image argument came from the name attribute of our form
        $image = $request->file('image');
        // time() returns time/seconds since UNIX epoch
        // getClientOriginalExtension() returns the file extension
        $image_name =time(). "." .$image->getClientOriginalExtension();
        // dd($image_name);
        $destination = "images/";

        // move(destination_path, file_name);
        $image->move($destination, $image_name);
        $item->image_url =$destination.$image_name;
        $item->save();

        Session::flash('success_message', 'Item added successfully');
        return  redirect('/catalog');
    }

    public function showEditForm($id) {
        $item = Item::find($id);
        // dd($item);
        $categories = Category::all();
        $genders = Gender::all();
        return view('items.edit_form', compact('item', ['categories', 'genders']));
    }

    


     public function saveEdit($id, Request $request) {
        $item = Item::find($id);
        // dd($request);
        $item->name = $request->name;
        $item->description = $request->description;
        $item->price_rent = $request->price_rent;
        $item->price_purchase = $request->price_purchase;
        $item->stocks = $request->number;
        $item->category_id = $request->category;
        $item->gender_id = $request->gender_id;
        $item->size_id = $request->size_id;
        // image argument came from the name attribute of our form
            if($request->file('image') != null) {
            $image = $request->file('image');
            // time() returns time/seconds since UNIX epoch
            // getClientOriginalExtension() returns the file extension
            $image_name =time(). "." .$image->getClientOriginalExtension();
            // dd($image_name);
            $destination = "images/";

            // move(destination_path, file_name);
            $image->move($destination, $image_name);
            $item->image_url =$destination.$image_name;
        }
        $item->save();
        return  redirect('/catalog');
    }
    public function addToCart($id, Request $request) {
        if(Session::has('cart')) {
            $cart = Session::get('cart');
        } else {
            $cart = [];
        }

        if(isset($cart[$id])) {
            $cart[$id]+=$request->quantity;
        } else {
            $cart[$id]= $request->quantity;
        }

        // if(isset($cart[$id])) {
        //     $cart[$id]+=$request->price;
        // } else {
        //     $cart[$id]=$request->price;
        // }


        Session::put('cart', $cart);

        $item = Item::find($id);
        Session::flash('success_message_cart', "$request->quantity of $item->name has been successfully added to your cart");

        // return array_sum(Session::get('cart'));
        // Session::forget('cart');
        return redirect('/catalog');

    }

    public function showCart() {
        $item_cart = [];
        $total = 0;

        $line_items = [];
        $user = Auth::user();

        if (Session::has('cart')) {
            $cart = Session::get('cart');
            
            
            // dd($cart);
        foreach ($cart as $id => $quantity) {
            $item= Item::find($id);
            
            // if($quantity > $item->stocks){
            //     $quantity = $item->stocks;
            // }
            $item->quantity = $quantity;
            
            $item->subtotal = $item->price_purchase * $quantity;
            $total += $item->subtotal; 
    
            $item_cart[] = $item;


            $line_items[] = [
            'name' => $item->name,
            'description' => $item->description,
            'images' => ['https://besticoforyou.com/wp-content/uploads/2018/11/Professional-Tips-for-Creating-A-Token-besticoforyou.jpg'],
            'amount' => str_replace(".", "", $item->price_purchase),
            

            'currency' => 'usd',
            'quantity' => (int)$quantity,

            // dd($line_items);
        
        
        ];
    }
        \Stripe\Stripe::setApiKey('sk_test_UemE12SQrw7mQaw3XUfMi5wQ00LIEupqoV');

        $stripe_session = \Stripe\Checkout\Session::create([
          'payment_method_types' => ['card'],
          'customer_email' => $user->email,
          'line_items' => $line_items,
          'success_url' => 'http://localhost:8000/transaction_complete',
          'cancel_url' => 'http://localhost:8000/menu/mycart',
          ]);
        $CHECKOUT_SESSION_ID = $stripe_session['id'];
        return view("items.cart_content", compact('item_cart', 'total', 'CHECKOUT_SESSION_ID'));
        

        //     $cart_session = \cart\Checkout\Session::create([
        //   'payment_method_types' => ['card'],
        //   'customer_email' => $user->email,
        //   'line_items' => $line_items,
        //   'success_url' => 'http://localhost:8000/transaction_complete',
        //   'cancel_url' => 'http://localhost:8000/menu/mycart',
        // ]);
    // }
            // $CHECKOUT_SESSION_ID = $cart_session['id'];
            //  return view("items.cart_content", compact('item_cart', 'total', 'CHECKOUT_SESSION_ID'));
    }
            return view("items.cart_content", compact('item_cart', 'total'));


    }

     public function clearCart() {
        Session::forget('cart');
        // $items = Item::all();
        Session::flash('success_message', 'Your cart is now empty!');
        return redirect('/catalog');
    }

    public function deleteCartItem ($id) {
        // dd($id);
        $item = Item::find($id);
        Session::forget("cart.$id");
        Session::flash('success_message', "$item->name was successfully deleted!");
        if (Session::get('cart') != []) {
            return back();
        } else {
            Session::forget('cart');
            return back();
        }
    }

    public function changeItemQty($id, Request $request) {
        

        $cart = Session::get('cart');
        $cart[$id] = $request->quantity;
        //dd($cart[$id]);
        // dd($request->quantity);

        Session::put('cart', $cart);
        // return view('items.cart_content');
        return back();
    }

    public function checkout(){
       $order = new Order;
       // we need to make sure that the user that's trying to checout is logged in, else, we would encounter an error with auth::user
       $order->user_id = Auth::user()->id;
       $order->total = 0; //set initial value 0
       $order->status_id = 1;
       $total = 0;
       $order->save();
    //    dd(Session::get('cart'));
       foreach (Session::get('cart') as $item_id => $quantity) {
           // dd($quantity);
           // dd($item_id);
           $order->items()->attach($item_id,['quantity'=>$quantity]);
           // attach method means to insert to the pivot table
           // syntax attach(other fk,[other columns you want to include])
       // dd($order);

           // update order total
           $item = Item::find($item_id);
        //    dd($item->stocks);
           $total += $item->price_purchase * $quantity;
           $item->stocks = $item->stocks-$quantity;
           $item->save();



       }

       $order->total=$total;
       $order->save();
       // $order is the reference to the Order model, items()->attach is a function that allows us to insert the item to our pivot table item_order for that specific order_id along with any pivot columns that we want to include, in this case, quantity column
       //attach id from partner table,pivot columns

       //remove the current session cart and return to catalog
       Session::forget('cart');
       // Session::flash()
       return view('orders.order_confirmation', compact('order'));

       // return back();

    }

  

    public function ShowOrders()
    {
        $user = Auth::user();
        // dd($user->id);
        $orders = Order::where('user_id', $user->id)->get();
        $statuses = Status::all();
        return view('orders.order_history', compact('orders'));
    }



}