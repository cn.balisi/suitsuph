<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Gender;
use Items;
use App;

class GenderController extends Controller
{
    public function findItems($id){
    	$gender = Gender::find($id);
    	//dd($category);
    	$items = $gender->items;
    	//this enables us to SELECT ALL ITEMS for category
    	//this is the effect of the hasMany items() relationship declaration
    	return view('items.catalog', compact('items'));
    }
}
