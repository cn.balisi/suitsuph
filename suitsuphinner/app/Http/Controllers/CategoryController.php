<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Category;
use Items;
use App;

class CategoryController extends Controller
{
    public function findItems($id){
    	$category = Category::find($id);
    	//dd($category);
    	$items = $category->items;
    	//this enables us to SELECT ALL ITEMS for category
    	//this is the effect of the hasMany items() relationship declaration
    	return view('items.catalog', compact('items'));
    }
}
