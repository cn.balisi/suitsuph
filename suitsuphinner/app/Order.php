<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    public function items() {
   	return $this->belongsToMany("\App\Item")->withPivot("quantity")->withTimeStamps();
   }
   // belongsToMany links it(the order) to the items table via the item_order pivot table
   // this is the reason why naming convention is important specially frameworks
   // withPivot contains all the columns that are not foreign keys and are not ids and timestamp in the pivot table
   // if you have more than one pivot column, use comma to separate them ->withPivot('col1','col2')
   // withTimeStamp() populates the timestamp as soon as an entry for the item_order is created
   public function status() {
   	return $this->belongsTO('\App\Status');
   }

 
}
