@extends('layouts.app');

<title>ORDER CONFIRMATION || SUITS.U</title>
    <link rel="icon" alt class="titlelogo"type="images/png" href="../images/suitsulogoblack2.png">

<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
 <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.2/css/all.css" integrity="sha384-oS3vJWv+0UjzBfQzYUhtDYW+Pj2yciDJxpsK1OYPAYjqT085Qq/1cq5FLXAZQ7Ay" crossorigin="anonymous">

<!-- @section('title', 'Order Confirmation') -->

@section('content')
	<div class="container orderconfi">
		<div class="jumbotron">
			<h2>Thank you for shopping with us!</h2>
			<p>Your payment will be verified within the day. Please proceed to <a href="{{ url('/orders') }}">your order history</a>
			to check the status of your order.</p>
		</div>

	</div>

@endsection