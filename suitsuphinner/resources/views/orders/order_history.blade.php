@extends('layouts.app');
@section('title', 'Order Confirmation')
@section('content')
<div class="container">
	<div class="row">
		<div class="col">
			<table class="table table-hover">
				<thead>
					<tr>
						
						<th scope="col">Order #</th>
						<th scope="col">Date Purchased</th>
						<th scope="col">Total</th>
						<th scope="col">Stats</th>
					</tr>
				</thead>
				<tbody>
						@foreach($orders as $order)
					<tr>
						<td>{{ $order->id}}</td>
						<td>{{ $order->created_at}}</td>
						<td>{{ number_format($order->total,2)}}</td>
						<td>{{ $order->status->name}}</td>
					</tr>
					@endforeach
				</tbody>
			</div>
		</div>
	</div>
</div>
@endsection