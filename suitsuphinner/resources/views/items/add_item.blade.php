{{-- {{ $categories }}

{{ $genders }}  --}}

@extends('layouts.app')

@section('content')

<div class="addit">	
	
	<div class="additem container">
		<h1 class="additem">Add New Item</h1>
		<div class="row">
			<div class="col-md-8 mx-auto">
				
				<form action="/menu/add" method="POST" enctype="multipart/form-data">
					@csrf

					<div class="form-group">
						<label for="name">Name:</label>
						<input type="text" name="name" id="name" class="form-control">
					</div>

					<div class="form-group">
						<label for="description">Description:</label>
						<input type="text" name="description" id="description" class="form-control">
					</div>

					<div class="form-group">
						<label for="price_rent">Price rent:</label>
						<input type="text" name="price_rent" id="price_rent" class="form-control">
					</div>

					<div class="form-group">
						<label for="price_purchase">Price purchase:</label>
						<input type="text" name="price_purchase" id="price_purchase" class="form-control">
					</div>

					<div class="form-group">
						<label for="stocks">Stocks:</label>
						<input type="number" name="number" id="stocks" class="form-control" min="1">
					</div>

					<div class="form-group">
						<label for="size">Size:</label>
						<input type="text" name="size" id="size" class="form-control">
					</div>


					<div class="form-group">
						<label for="gender">Gender:</label>
						<select name="gender" id="gender" class="form-control">
							@foreach($genders as $gender)
								<option value="{{ $gender->id }}"> {{ $gender->name }}</option>
							@endforeach
						</select>
					</div>

					<div class="form-group">
						<label for="category">Category:</label>
						<select name="category" id="category" class="form-control">
							@foreach($categories as $category)
								<option value="{{ $category->id }}"> {{ $category->name }}</option>
							@endforeach
						</select>
					</div>

					<div class="form-group">
						<label for="image">Upload Image</label>
						<input type="file" name="image" id="image" class="form-control">
					</div>

					<button class="btn btn-success" type="submit">Add New Item</button>

				</form>

			</div> {{-- end cols --}}
		</div> {{-- end row --}}
	</div> {{-- end container --}}
	</div>
@endsection