{{-- {{ dd($items) }} --}}
@extends('layouts.app')
@section('content')

@if(Session::get('cart') != [])
<table class="table text-center col-md-8 offset-2">
	<thead>
		<tr>
			<th scope="col">Items</th>
			<th scope="col">Quantity</th>
			<th scope="col">Price_purchase</th>
			<th scope="col">Subtotal</th>
			<th scope="col">Action</th>
		</tr>
	</thead>
	<tbody>
		{{-- 	{{ dd($item_cart) }} --}}
		@foreach($item_cart as $item)
		<tr>
			<td> {{ $item->name }}</td>
			<td>
			<form method="post" id="update_quantity{{$item->id}}">
                  @csrf
                  {{ method_field('PATCH')}}

                  <div class="input-group mb-3">
                    <div class="input-group-prepend">
                      <button type="button" class="input-group-text rounded-0" onclick="minus({{ $item->id }})">&#8722;</button>
                    </div>
                    {{-- <input type="text" value="{{$item->id}}"> --}}
                   <input type="number" min="1" value="{{ $item->quantity }}" name="quantity" class="text-center" id="quantity{{$item->id}}" style="width:15%;"readonly>
                    <div class="input-group-append">
                      <button type="button" class="input-group-text rounded-0" onclick="plus({{ $item->id}})">+</button>
                    </div>
                  </div>
                </form>
			</td>
			<td>₱{{ number_format($item->price_purchase,2) }}</td>
			<td>₱{{ number_format($item->subtotal,2) }}</td>
			<td>{{-- <button class="btn btn-danger"> <i class="fas fa-trash-alt"></i> --}}
				<button class="btn btn-danger"
				onclick="openDeleteModal('{{ $item->id }}', '{{ $item->name }}')"
				data-toggle="modal"
				data-target="#delete_modal"> <i class="fas fa-trash-alt"></i>
				
				</button>
			</td>
		</tr>
		
		@endforeach
		
		<tr>
			<td></td>
			<td></td>
			<td></td>
			<td>₱{{ number_format($total,2) }}</td>
			<td>


				<div class="row">
					<div class="col">
						<div class="d-flex flex-row">
							<a href="/catalog" class="btn btn-primary mr-2">Back To Shopping</a>			
							<form action="/cart/clearcart" method="post">
								@csrf
								{{ method_field('DELETE') }}
								<button type="submit" class="btn btn-outline-danger">Clear Cart</button>
							</form>
							
						</div>
						<div>
							<a onclick="pay()" class="btn btn-success mr-2">Checkout COD</a>
						</div>

					</div>
				</div>
				
			</td>
		</tr>
	</tbody>
</table>

	{{-- <form action="/mycart/{{$item->id}}/delete" method="post">
		@csrf
		{{ method_field('DELETE') }}
		<button href="#" class="btn btn-danger" type="submit">Delete</button>
	</form> --}}
	@else
	<h1>test</h1>
	@endif
	
	@if($item_cart == null)
	<div class="container jumbotron emptycart">
		<div class="row">
			<div class="col-12 text-center">
				<h4>Your cart is empty</h4>
			</div>
		</div>
		<div class="row">
			<div class="col-12 text-center">
				<a href="/catalog" class="btn btn-dark shopnow">Shop Now!</a>
			</div>
		</div>
	</div>
	@endif
	{{-- DELETE ITEM MODAL --}}
	<div class="modal fade text-dark" tabindex="-1" role="dialog" id="delete_modal">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<h5 class="modal-title">Delete Item</h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
					</button>
				</div>
				<div class="modal-body ">
					<p>Are you sure you want to delete ?</p>
				</div>
				<div class="modal-footer">
					<div class="d-flex flex-row">
			          <form method="post" id="delete_form">
			            @csrf
			            {{ method_field('DELETE')}}
			            <button class="btn btn-danger" 
			            type="submit">Delete Item</button>
			          </form>
					<button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
					</form>
				</div>
			</div>
		</div>
	</div>

	@if(Session::has('cart'))
	<script src="https://js.stripe.com/v3/"></script>

	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.0/jquery.min.js"></script>
	

	<script>

		const stripe = Stripe('pk_test_weXddW8LB0NV1tJZZygG0QDy00y5XkF3Sp');

		function pay() {
			// alert('hello');

			stripe.redirectToCheckout({
			  sessionId: '{{$CHECKOUT_SESSION_ID}}'
			}).then((result) => {
				$('error_modal').modal('show');

			});
		}
	</script>
	@endif
	<script>
		function openDeleteModal(id, name) {
		$('#delete_form').attr('action', '/mycart/' + id + '/delete');
		$('#delete_question').html("Do you want to delete " + name + "?");

		document.querySelectorAll(".subDel").forEach(function(subD) {
			console.log(subD.parentNode.childNodes[2])
		})
		}
	</script>
	
	

@endsection


