<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>SUITS.U</title>
    <link rel="icon" alt class="titlelogo"type="images/png" href="../images/suitsulogoblack2.png">

    <!-- Scripts -->
     <script src="{{ asset('js/app.js') }}"></script>
     
     <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">

   
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Pacifico&display=swap" rel="stylesheet">

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.2/css/all.css"integrity="sha384-oS3vJWv+0UjzBfQzYUhtDYW+Pj2yciDJxpsK1OYPAYjqT085Qq/1cq5FLXAZQ7Ay" crossorigin="anonymous">


</head>
<body>
    <div id="navup">
        <div class="navbar">
        <nav class="navbar navbar-expand-lg shadow-sm navbar-dark fixed-top">
            <div class="container">
                <a class="navbar-brand" href="{{ url('/') }}">
                  <img src="../images/suitsulogowhite2.png" alt class="logo">
                </a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="{{ __('Toggle navigation') }}">
                    <span class="navbar-toggler-icon"></span>
                </button>

                
                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                  
                     

                    <!-- Right Side Of Navbar -->
                    <ul class="navbar-nav ml-auto">
                        <!-- Authentication Links -->
                        

                         <li class="nav-item">
                          <a class="navbar" href="{{ url('/') }}">HOME</a>
                          </li>
                          <li class="nav-item">
                          <a class="navbar" href="{{ url('/catalog') }}">COSTUMES</a>
                          </li>
                          <li class="nav-item">
                          <a class="navbar" href="{{ url('/home') }}">CONTACTS</a>
                          </li>
                          @auth
                        @if(auth()->user()->is_admin == 1)
                        <li class="nav-item">
                          <a class="navbar" href="{{ url('/menu/add') }}">ADD ITEM</a>
                          </li>
                        @endif
                        @endauth
                        @auth
                        @if(auth()->user()->is_admin != 1)
                           <li class="nav-item">
                                <a class="navbar" href="{{  url('/menu/mycart') }}">CART
                                    <span class="badge badge-dark text-light text-center">
                                    @if(Session::has('cart'))
                                    {{ array_sum(Session::get('cart'))}}
                                    @endif
                                    </span>
                                </a>
                            </li>
                            @endif
                            @endauth
                            <!-- <li class="serchbtn active"> </span> <i class="fas fa-search" aria-hidden></i> -->
                               @guest

                         <li class="nav-item dropdownAccount">
                                <a id="navbardropdownaccount" class="navbar dropdown-toggle" href="#"  data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                                    MY ACCOUNT <span class="caret"></span>
                                </a>
                                <div class="dropdown-menu  dropdown-menu-right" aria-labelledby="navbarDropdown">
                            
                                <a class="nav-link text-dark text-center dropdown-item" href="{{ route('login') }}">LOGIN</a>
                            

                            @if (Route::has('register'))
                                
                                    <a class="nav-link text-dark text-center dropdown-item" href="{{ route('register') }}">REGISTER</a>
                                
                            @endif
                          

                        @else
                             <li class="nav-item dropdown">
                                <a id="navbarDropdown" class="navbar dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                                  LOGOUT   <span class="caret"></span>
                                </a>

                                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                                    <a class="dropdown-item" href="{{ route('logout') }}"
                                       onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                        {{ Auth::user()->name }}
                                    </a>

                                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="">
                                        @csrf
                                    </form>
                                </div>
                            </li>
                        @endguest

                       
                        </div>
                 

                       
                    </ul>

                </div>
                
                   
                    

                </div>
                                    
            </div>
        </nav>

        <main class="py-0 px-0 container-fluid">
            @yield('content')
          <footer class="footer fixed-bottom text-center py-0">
            <div class="row">
              <div class="col-12 social-media">
                <ul id="social-media">
                <a class="social-media" href="https://www.facebook.com/apple.nitullano/"><i class="fab fa-facebook-square"></i></a>
               <a class="social-media" href="https://gitlab.com/cn.balisi/"><i class="fab fa-gitlab"></i></a>
                <a class="social-media" href="https://www.instagram.com/toyotailovedeals/"><i class="fab fa-instagram"></i></a>
                </ul>
          <p class="text-left1">
            SUITS.U 2019 &copy; Copyright CNB
          </p>
          </footer>
            @yield('title')
        </main>

         @yield('script')
        </div>  
    </div>
   
    
   
          <!-- jQuery library -->
    <!-- <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.0/jquery.min.js"></script> -->

    <!-- Popper JS -->
    <!-- <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script> -->

    <!-- Latest compiled JavaScript -->
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script> 
    

    <!-- Fonts -->
    


</body>
</html>
