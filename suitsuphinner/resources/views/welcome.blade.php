<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>SUITS.U</title>
        <link rel="icon" alt class="titlelogo"type="images/png" href="../images/suitsulogoblack2.png">

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">

        <!-- Styles -->

        <link href="{{ asset('css/app.css') }}" rel="stylesheet">
        <style>
            html {
               /*  background: url("../images/homepic.jpg");*/
               /* background-color: black;*/
                color: #636b6f;
                font-family: 'Nunito', sans-serif;
                font-weight: 200;
                height: 100vh;
                margin: 0;
            }

            .full-height {
                height: 100vh;
            }

            .flex-center {
                align-items: center;
                display: flex;
                justify-content: center;
            }

            .position-ref {
                position: relative;
            }

            .top-right {
                position: absolute;
                right: 5px;
                top: 18px;
            }

            .content {
                margin-top: 20%;
                text-align: center;
            }

            .title {
                font-size: 20px;
                margin-top: 40%;
            }

            .links > a {
                color: black;
                padding: 0 50px;
                font-size: 13px;
                font-weight: 600;
                letter-spacing: .1rem;
                text-decoration: none;
                text-transform: uppercase;
            }

        
            .m-b-md {
                margin-bottom: 30px;
            }
        </style>
    </head>
    <body>
        <div class="wel container-fluid">
        <div class="flex-center position-ref full-height">
            <!-- @if (Route::has('login'))
                <div class="top-right links">
                      
                    @auth
                        <a href="{{ url('/home') }}">Home</a>
                    @else
                        <a href="{{ route('login') }}">Login</a>

                        @if (Route::has('register'))
                            <a href="{{ route('register') }}">Register</a>
                        @endif
                    @endauth
                    <a href="{{ url('/catalog') }}">Catalog</a>

                    <a href="{{ url('/menu/mycart') }}">Cart</a>
                </div>
            @endif -->

            <div class="content">
                 <div class="content">
                <p class="title m-b-md">
                    Costumes & Suits for Performances and Events 
                </p>

              <div class="dest links">
                    <a href="{{ url('/home') }}">RENT</a>
                    <a href="{{ url('/catalog') }}">PURCHASED</a>
                    <!-- <a href="https://laravel-news.com">News</a> -->
                    <!-- <a href="https://blog.laravel.com">Blog</a> -->
                    <!-- <a href="https://nova.laravel.com">Nova</a> -->
                    <!-- <a href="https://forge.laravel.com">Forge</a> -->
                    <!-- <a href="https://github.com/laravel/laravel">GitHub</a> -->
                </div>
            </div>
        </div>
        </div>
        </div>
    </body>
</html>
